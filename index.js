(function() {
    var emojies;
    var selectedCategory = "face-emoji";

    function createInputElement() {
        var inputWrapper = document.createElement('div');
        inputWrapper.setAttribute('class', 'input-wrapper');

        var inputfield = document.createElement('input');
        inputfield.setAttribute('id', 'showClickedEmo');

        var showEmojibtn = document.createElement('span');
        showEmojibtn.setAttribute('class', 'show-btn');
        showEmojibtn.innerHTML = "EMO";

        var emoWrapper = document.createElement('span');
        emoWrapper.setAttribute('class', 'emo-wrapper');

        var emoField = document.createElement('span');
        emoField.setAttribute('class', 'showMeEmos');

        var emoCategories = document.createElement('span');
        emoCategories.setAttribute('class', 'emo-categories');

        // make a loop to create buttons
        // based on categorie array
        var emoCategorie1 = document.createElement('button');
        emoCategorie1.setAttribute('class', 'toggleEmos');
        emoCategorie1.setAttribute('id', 'face-emoji');
        emoCategorie1.innerHTML = "Smiles!"

        var emoCategorie2 = document.createElement('button');
        emoCategorie2.setAttribute('class', 'toggleEmos');
        emoCategorie2.setAttribute('id', 'animals-nature-emoji');
        emoCategorie2.innerHTML = "Nature!"

        var emoCategorie3 = document.createElement('button');
        emoCategorie3.setAttribute('class', 'toggleEmos');
        emoCategorie3.setAttribute('id', 'food-drinks');
        emoCategorie3.innerHTML = "Foods&Drinks!"

        var emoCategorie4 = document.createElement('button');
        emoCategorie4.setAttribute('class', 'toggleEmos');
        emoCategorie4.setAttribute('id', 'activity-emoji');
        emoCategorie4.innerHTML = "Activities!"

        var emoCategorie5 = document.createElement('button');
        emoCategorie5.setAttribute('class', 'toggleEmos');
        emoCategorie5.setAttribute('id', 'travel-places-emoji');
        emoCategorie5.innerHTML = "travel places!"

        var emoCategorie6 = document.createElement('button');
        emoCategorie6.setAttribute('class', 'toggleEmos');
        emoCategorie6.setAttribute('id', 'object-emoji');
        emoCategorie6.innerHTML = "Objects!"

        var emoCategorie7 = document.createElement('button');
        emoCategorie7.setAttribute('class', 'toggleEmos');
        emoCategorie7.setAttribute('id', 'symbol-emoji');
        emoCategorie7.innerHTML = "Symbols!"

        var emoCategorie8 = document.createElement('button');
        emoCategorie8.setAttribute('class', 'toggleEmos');
        emoCategorie8.setAttribute('id', 'flag-emoji');
        emoCategorie8.innerHTML = "Flags!"

        inputWrapper.append(inputfield);
        inputWrapper.append(showEmojibtn);

        emoWrapper.append(emoField);

        emoCategories.append(emoCategorie1);
        emoCategories.append(emoCategorie2);
        emoCategories.append(emoCategorie3);
        emoCategories.append(emoCategorie4);
        emoCategories.append(emoCategorie5);
        emoCategories.append(emoCategorie6);
        emoCategories.append(emoCategorie7);
        emoCategories.append(emoCategorie8);

        emoWrapper.append(emoCategories);

        inputWrapper.append(emoWrapper);

        var inputElements = document.getElementsByTagName('input');

        for(var i = 0; i < inputElements.length; i += 1) {
            inputElements[i].parentNode.replaceChild(inputWrapper, inputElements[i]);
        }

        replaceDone();
    }
    createInputElement();

    function replaceDone() {
        var elem = document.getElementsByClassName('showMeEmos')[0]; // for now I know there is only one;
        var categories = document.getElementsByClassName('emo-categories')[0]; // for now I know there is only one;
        var output = document.getElementById('showClickedEmo');

        function applyEmo() {
            var i;
            elem.innerHTML = "";
            for (i = 0; i < emojies.length; i++) {
                var elementSpan = document.createElement('span');
                elementSpan.setAttribute('id', 'emoji-'+emojies[i].no);
                var emo = emojies[i].codes.split(' ');

                if (emo.length > 2) {
                    for(var j = 0; j < emo.length; j ++) {
                        console.log(emo[j])
                        elementSpan.innerHTML = '&#X'+emo[j]+';';
                    }
                } else {
                    elementSpan.innerHTML = emojies[i].char;
                }
                elem.append(elementSpan);
            };
        }

        function clickedEmo(emojiId) {
            var emoji = document.getElementById(emojiId).innerHTML;

            var values = output.value;
            values += emoji;
            output.value = values;
        };

        elem.addEventListener('click', function(e) {
            if (e.target.id !== '') {
                console.log(e.target.id)
                clickedEmo(e.target.id);
            }
        });

        function loadJSON(callback) {

            var xobj = new XMLHttpRequest();
            xobj.overrideMimeType("application/json");
            xobj.open('GET', selectedCategory+'.json', true);
            xobj.onreadystatechange = function () {

                if (xobj.readyState === 4 && xobj.status === 200) {
                    // Required use of an anonymous callback as .open will NOT return a value
                    // but returns undefined in asynchronous mode
                    callback(xobj.responseText);
                }
            };
            xobj.send(null);
        };

        var classElem = document.getElementsByClassName('toggleEmos');

        for(var i = 0; i < classElem.length; i += 1) {
            classElem[i].addEventListener('click', loadEmos, false);
        }

        function loadEmos(e) {
            if (e) {
                selectedCategory = e.target.id;
            }

            var cachedEmo = sessionStorage.getItem(selectedCategory);

            if (cachedEmo) {
                emojies = JSON.parse(cachedEmo);
                applyEmo();
            } else if (selectedCategory) {
                console.log('not cached');
                loadJSON(function(response) {
                    emojies = JSON.parse(response);
                    sessionStorage.setItem(selectedCategory, JSON.stringify(emojies));
                    applyEmo();
                });
            }
        }

        var classElem = document.getElementsByClassName('show-btn');

        for(var i = 0; i < classElem.length; i += 1) {
            classElem[i].addEventListener('click', toggleEmojies, false);
        }

        function toggleEmojies(e) {
            e.preventDefault();
            var isVisible = elem.style.display;
            loadEmos();
            if(isVisible === "none" || isVisible === "") {
                elem.style.display = "inline-block";
                categories.style.display = "inline-block"
            } else {
                categories.style.display = "none"
                elem.style.display = "none";
            }
        };
    }
})();
